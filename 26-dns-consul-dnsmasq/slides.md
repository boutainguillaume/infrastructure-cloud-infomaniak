%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Push our dns on our network and laptop

<br>

FIX disk issue : create the user !!!

```
- { disk: '/dev/sdb', path: '{{ consul_dir_master_data }}', owner: "consul" }
```


-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Push our dns on our network and laptop

<br>

For our dns : multiple solution (with or without systemd)

Be careful on the consul dns port = 8600

My solution : use dnsmasq

  * install dnsmasq

  * disable and stop resolved

  * edit /etc/resolv/conf

  * check /etc/hosts

  * edit dnsmasq configuration

  * start dnsmasq

  * add it on all of our servers

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Push our dns on our network and laptop

<br>

Install dnsmasq

```
- name: Install utils
  apt:
    name: dnsmasq,dnsutils
    state: present
    update_cache: yes
    cache_valid_time: 3600
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Push our dns on our network and laptop

<br>

Disable and stop resolved

```
- name: stop systemd-resolved
  systemd:
    name: systemd-resolved
    state: stopped
    enabled: false
    masked: true
  ignore_errors: true
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Push our dns on our network and laptop

<br>

Edit /etc/resolv.conf

```
- name: edit resolv.conf
  template:
    src: resolv.conf.j2
    dest: /etc/resolv.conf
    mode: 0644
    owner: root
    group: root
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Push our dns on our network and laptop

<br>

Edit /etc/hosts

```
- name: add hostname in /etc/hosts
  lineinfile:
    dest: /etc/hosts
    line: "127.0.0.1 {{ ansible_hostname }}"
    state: present
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Push our dns on our network and laptop

<br>

Edit /etc/hosts

```
- name: add dnsmasq config
  template:
    src: 10-consul.conf.j2
    dest: /etc/dnsmasq.d/10-consul.conf         
    mode: 0755
  notify: restart_dnsmasq
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Push our dns on our network and laptop

<br>

Start dnsmasq

```
- name: dnsmasq started
  systemd:
    name: dnsmasq
    state: started
    enabled: true
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Push our dns on our network and laptop

<br>

Template 10-consul.conf.j2 dnsmasq

```
server=/.consul/127.0.0.1#8600
server=1.1.1.1
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Push our dns on our network and laptop

<br>

Template resolv.conf.j2 dnsmasq

```
nameserver 127.0.0.53
options edns0 trust-ad
search .
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Push our dns on our network and laptop

<br>

The dnsmasq handler

```
- name: restart_dnsmasq
  systemd:
    name: dnsmasq
    state: restarted
    enabled: yes
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Push our dns on our network and laptop

<br>

Add dns-common

And push agent and dns on openvpn

```
- name: consul agent
  hosts: all,!meta-app_consul
  become: true
  roles:
    - consul
    - dns-common
```

