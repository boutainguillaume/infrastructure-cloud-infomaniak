%title: Infra Cloud Infomaniak
%author: xavki


██╗   ██╗███████╗ ██████╗████████╗ ██████╗ ██████╗ 
██║   ██║██╔════╝██╔════╝╚══██╔══╝██╔═══██╗██╔══██╗
██║   ██║█████╗  ██║        ██║   ██║   ██║██████╔╝
╚██╗ ██╔╝██╔══╝  ██║        ██║   ██║   ██║██╔══██╗
 ╚████╔╝ ███████╗╚██████╗   ██║   ╚██████╔╝██║  ██║
  ╚═══╝  ╚══════╝ ╚═════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝


-----------------------------------------------------------------------------------------------------------                                       

# Loki & Grafana : logql

<br>

```
{type="journald"} |= `error` | json | line_format `{{.host}} / {{.SYSLOG_IDENTIFIER}} / {{ .message }}`
```


```
{instance="traefik1", type="traefik_access"}  | json | entryPointName = `https` | line_format "{{ .time }} / {{ .DownstreamStatus }} / {{ .RequestHost }} / {{ .ClientAddr }} / {{ .TLSVersion }} / {{ .RequestMethod }} / {{ .RequestProtocol }} / {{ .RequestPath }}"
```