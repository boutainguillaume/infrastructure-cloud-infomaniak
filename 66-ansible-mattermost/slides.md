%title: Infra Cloud Infomaniak
%author: xavki


███╗   ███╗ █████╗ ████████╗████████╗███████╗██████╗ ███╗   ███╗ ██████╗ ███████╗████████╗
████╗ ████║██╔══██╗╚══██╔══╝╚══██╔══╝██╔════╝██╔══██╗████╗ ████║██╔═══██╗██╔════╝╚══██╔══╝
██╔████╔██║███████║   ██║      ██║   █████╗  ██████╔╝██╔████╔██║██║   ██║███████╗   ██║   
██║╚██╔╝██║██╔══██║   ██║      ██║   ██╔══╝  ██╔══██╗██║╚██╔╝██║██║   ██║╚════██║   ██║   
██║ ╚═╝ ██║██║  ██║   ██║      ██║   ███████╗██║  ██║██║ ╚═╝ ██║╚██████╔╝███████║   ██║   
╚═╝     ╚═╝╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚══════╝╚═╝  ╚═╝╚═╝     ╚═╝ ╚═════╝ ╚══════╝   ╚═╝   


-----------------------------------------------------------------------------------------------------------                                       

# Mattermost : installation with ansible

<br>

Install user & group

```
- name: create group mattermost
  group:
    name: mattermost
    system: yes
    state: present

- name: create user mattermost
  user:
    name: mattermost
    system: yes
    shell: /sbin/nologin
    state: present
```

-----------------------------------------------------------------------------------------------------------                                       

# Mattermost : installation with ansible

<br>

Check if exist and version

```
- name: check if mattermost exists
  stat:
    path: /opt/mattermost/bin/mattermost
  register: __mattermost_exists
```

-----------------------------------------------------------------------------------------------------------                                       

# Mattermost : installation with ansible

<br>

Check if exist and version

```
- name: if mattermost exists get version
  shell: "cat /etc/systemd/system/mattermost.service | grep Version | sed s/'.*Version '//g"
  register: __get_mattermost_version
  when: __mattermost_exists.stat.exists == true
  changed_when: false
 ```

-----------------------------------------------------------------------------------------------------------                                       

# Mattermost : installation with ansible

<br>

Create data directory

```
- name: create directory for mattermost data 
  file:
    path: "/opt/mattermost/{{ item }}"
    state: directory
    mode: 0750
    owner: mattermost
    group: mattermost
  loop:
  - data
```

-----------------------------------------------------------------------------------------------------------                                       

# Mattermost : installation with ansible

<br>

Install the binary

```
- name: download mattermost
  unarchive: 
    src: "https://releases.mattermost.com/{{ mattermost_version }}/mattermost-{{ mattermost_version }}-linux-amd64.tar.gz"
    dest: /tmp/
    remote_src: yes
  when: __mattermost_exists.stat.exists == False or not __get_mattermost_version.stdout == mattermost_version
```

-----------------------------------------------------------------------------------------------------------                                       

# Mattermost : installation with ansible

<br>

Install the binary

```
- name: move the binary to the final destination
  copy:
    src: "/tmp/mattermost/"
    dest: "/opt/mattermost"
    owner: mattermost
    group: mattermost
    mode: 0750
    remote_src: yes
  when: __mattermost_exists.stat.exists == False or not __get_mattermost_version.stdout == mattermost_version
```

-----------------------------------------------------------------------------------------------------------                                       

# Mattermost : installation with ansible

<br>

Install the systemd service

```
- name: mattermost systemd file
  template:
    src: "mattermost.service.j2"
    dest: "/etc/systemd/system/mattermost.service"         
    mode: 0750
  notify: "reload_daemon_and_restart_mattermost"
```

-----------------------------------------------------------------------------------------------------------                                       

# Mattermost : installation with ansible

<br>

Push the configuration file

```
- name: mattermost configuration file
  template:
    src: "config.json.j2"
    dest: "/opt/mattermost/config/config.json"      
    mode: 0750
  notify: "reload_daemon_and_restart_mattermost"

- meta: flush_handlers
```

-----------------------------------------------------------------------------------------------------------                                       

# Mattermost : installation with ansible

<br>

Check if exist and version

```
- name: start mattermost
  systemd:
    name: mattermost
    state: started
    enabled: yes
```

-----------------------------------------------------------------------------------------------------------                                       

# Mattermost : installation with ansible

<br>

Variables

```
mattermost_version: 9.2.2
mattermost_database_uri: "postgres://{{ mattermost_database_connexion }}/mattermost?sslmode=disable\u0026connect_timeout=10\u0026binary_parameters=yes"
mattermost_database_connexion: ""
```

-----------------------------------------------------------------------------------------------------------                                       

# Mattermost : installation with ansible

<br>

The handler

```
- name: reload_daemon_and_restart_mattermost
  systemd:
    name: mattermost
    state: restarted
    daemon_reload: yes
    enabled: yes
```

-----------------------------------------------------------------------------------------------------------                                       

# Mattermost : installation with ansible

<br>

The systemd serive template

```
[Unit]
Description=Mattermost Version {{ mattermost_version }}
After=network.target

[Service]
Type=notify
ExecStart=/opt/mattermost/bin/mattermost
TimeoutStartSec=3600
KillMode=mixed
Restart=always
RestartSec=10
WorkingDirectory=/opt/mattermost
User=mattermost
Group=mattermost
LimitNOFILE=49152

[Install]
WantedBy=multi-user.target
```

-----------------------------------------------------------------------------------------------------------                                       

# Mattermost : installation with ansible

<br>

The playbook

```
- name: mattermost
  become: yes
  hosts: meta-app_mattermost
  roles:
    - volumes
    - mattermost
    - consul_services
  vars:
    volumes_disks:
      - { disk: '/dev/sdb', path: '/opt' }
    consul_services:
      - { 
        name: "mattermost", 
        type: "http", 
        target: "http://127.0.0.1:8065", 
        interval: "10s", 
        port: 8065,
        tags: [
            "traefik.enable=true",
            "traefik.http.routers.router-mattermost.entrypoints=http,https",
            "traefik.http.routers.router-mattermost.rule=Host(`m.xavki.fr`)",
            "traefik.http.routers.router-mattermost.service=mattermost",
            "traefik.http.routers.router-mattermost.middlewares=auth@file,to_https@file",
            "traefik.http.routers.router-mattermost.tls.certresolver=certs_gen"
          ]
      }
    mattermost_database_connexion: "xavki_mattermost:password@postgresql_primary.service.consul"
```