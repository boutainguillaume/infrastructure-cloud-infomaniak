%title: Infra Cloud Infomaniak
%author: xavki


██████╗  ██████╗ ███████╗████████╗ ██████╗ ██████╗ ███████╗███████╗ ██████╗ ██╗     
██╔══██╗██╔═══██╗██╔════╝╚══██╔══╝██╔════╝ ██╔══██╗██╔════╝██╔════╝██╔═══██╗██║     
██████╔╝██║   ██║███████╗   ██║   ██║  ███╗██████╔╝█████╗  ███████╗██║   ██║██║     
██╔═══╝ ██║   ██║╚════██║   ██║   ██║   ██║██╔══██╗██╔══╝  ╚════██║██║▄▄ ██║██║     
██║     ╚██████╔╝███████║   ██║   ╚██████╔╝██║  ██║███████╗███████║╚██████╔╝███████╗
╚═╝      ╚═════╝ ╚══════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚══════╝╚══════╝ ╚══▀▀═╝ ╚══════╝



-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part2


<br>

Purpose :

  * install the secondary (standby)

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part2


<br>

The playbook - postgresql replication

```
- name: install our postgresql replication
  become: yes
  hosts: postgresql1,postgresql2
  serial: 1
  roles:
    - databases/postgresql/postgresql_replication
  vars:
    postgresql_replication_data_dir: "/data/postgresql"
    postgresql_replication_primary: "postgresql1"
```


-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part2


<br>

Stop posgtresql on the secondary

```
- name: stop postgres on standby
  service:
    name: postgresql
    state: stopped
  when: ansible_hostname != postgresql_replication_primary and ansible_hostname not in __test_repmgr.stdout
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part2


<br>

Drop the PG_DATA on the secondary

```
- name: drop pg_data on standby
  file:
    path: "{{ postgresql_replication_data_dir }}/"
    state: absent
  when: ansible_hostname != postgresql_replication_primary  and ansible_hostname not in __test_repmgr.stdout
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part2


<br>

Recreate the PG_DATA on the secondary

```
- name: add pg_data on standby
  file:
    path: "{{ postgresql_replication_data_dir }}/"
    state: directory
    owner: postgres
    group: postgres
    mode: 0755  
  when: ansible_hostname != postgresql_replication_primary  and ansible_hostname not in __test_repmgr.stdout
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part2


<br>

Then sync the secondary with the primary

```
- name: sync standby
  shell: "repmgr -h {{ postgresql_replication_primary }} -U repmgr -d repmgr standby clone -c"
  become_user: postgres
  when: ansible_hostname != postgresql_replication_primary  and ansible_hostname not in __test_repmgr.stdout
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part2


<br>

Then start postgresql on the secondary

```
- name: start postgres on standby
  service:
    name: postgresql
    state: started
  when: ansible_hostname != postgresql_replication_primary  and ansible_hostname not in __test_repmgr.stdout
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part2


<br>

And register the standby

```
- name: register standby
  shell: repmgr standby register
  become_user: postgres
  when: ansible_hostname != postgresql_replication_primary  and ansible_hostname not in __test_repmgr.stdout
  notify: restart_repmgr
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part2


<br>

Flush handlers

```
- name: "Flush handlers"
  meta: flush_handlers
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part2


<br>

Add rsync script

```
- name: push pg_rsync.sh
  template:
    src: pg_sync.sh.j2
    dest: /opt/
    mode: 750
    owner: root
    group: root
  loop: "{{ groups[postgresql_replication_group] }}"
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part2


<br>

Add template (rsync script)