%title: Infra Cloud Infomaniak
%author: xavki


██████╗  ██████╗ ███████╗████████╗ ██████╗ ██████╗ ███████╗███████╗ ██████╗ ██╗     
██╔══██╗██╔═══██╗██╔════╝╚══██╔══╝██╔════╝ ██╔══██╗██╔════╝██╔════╝██╔═══██╗██║     
██████╔╝██║   ██║███████╗   ██║   ██║  ███╗██████╔╝█████╗  ███████╗██║   ██║██║     
██╔═══╝ ██║   ██║╚════██║   ██║   ██║   ██║██╔══██╗██╔══╝  ╚════██║██║▄▄ ██║██║     
██║     ╚██████╔╝███████║   ██║   ╚██████╔╝██║  ██║███████╗███████║╚██████╔╝███████╗
╚═╝      ╚═════╝ ╚══════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚══════╝╚══════╝ ╚══▀▀═╝ ╚══════╝



-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Exporter & Metrics

<br>

Some variables

```
postgresql_exporter_version: 0.15.0
postgresql_exporter_binary_path: /usr/local/bin
postgresql_exporter_user: postgres
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Exporter & Metrics

<br>

Create user

```
- name: create group postgresql_exporter
  ansible.builtin.group:
    name: "{{ postgresql_exporter_user }}"
    system: yes
    state: present
```

```
- name: create user postgresql_exporter
  ansible.builtin.user:
    name: "{{ postgresql_exporter_user }}"
    system: yes
    shell: /sbin/nologin
    state: present
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Exporter & Metrics

<br>

Check binary and version

```
- name: check if postgresql_exporter exists
  ansible.builtin.stat:
    path: "{{ postgresql_exporter_binary_path }}/postgres_exporter"
  register: __postgresql_exporter_exists

- name: if postgresql_exporter exists get version
  ansible.builtin.shell: "cat /etc/systemd/system/postgresql_exporter.service | grep Version | sed s/'.*Version '//g"
  register: __postgresql_exporter_get_version
  when: __postgresql_exporter_exists.stat.exists == true
  changed_when: false
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Exporter & Metrics

<br>

Install the binary

```
- name: download postgresql_exporter
  ansible.builtin.unarchive:
    src: "https://github.com/prometheus-community/postgres_exporter/releases/download/v{{ postgresql_exporter_version }}/postgres_exporter-{{ postgresql_exporter_version }}.linux-amd64.tar.gz"  
    dest: "/tmp/"
    remote_src: yes
    mode: 0750
    owner: "{{ postgresql_exporter_user }}"
    group: "{{ postgresql_exporter_user }}"
  when: __postgresql_exporter_exists.stat.exists == False or not __postgresql_exporter_get_version.stdout == postgresql_exporter_version
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Exporter & Metrics

<br>

Install the binary

```
- name: move the binary to the final destination
  copy:
    src: "/tmp/postgres_exporter-{{ postgresql_exporter_version }}.linux-amd64/postgres_exporter"
    dest: "{{ postgresql_exporter_binary_path }}"
    mode: 0750
    remote_src: yes
    owner: "{{ postgresql_exporter_user }}"
    group: "{{ postgresql_exporter_user }}"
  when: __postgresql_exporter_exists.stat.exists == False or not __postgresql_exporter_get_version.stdout == postgresql_exporter_version
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Exporter & Metrics

<br>

Install the binary

```
- name: clean
  file:
    path: "/tmp/postgres_exporter-{{ postgresql_exporter_version }}.linux-amd64/"
    state: absent
  when: __postgresql_exporter_exists.stat.exists == False or not __postgresql_exporter_get_version.stdout == postgresql_exporter_version
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Exporter & Metrics

<br>

Install the systemd service

```
- name: add systemd service for postgresql_exporter
  template:
    src: postgresql_exporter.service.j2
    dest: "/etc/systemd/system/postgresql_exporter.service"
    owner: root
    group: root
    mode: 0750
  notify: restart_postgresql_exporter
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Exporter & Metrics

<br>

Start service

```
- name: start postgresql_exporter
  service:
    name: postgresql_exporter
    state: started
    enabled: yes
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Exporter & Metrics

<br>

Check binary and version

```
[Unit]
Description=Postgresql Exporter Version {{ postgresql_exporter_version }}
Documentation=https://github.com/prometheus-community/postgres_exporter/releases
After=network-online.target
Requires=network-online.target

[Service]
User=postgres
Group=postgres
Environment=DATA_SOURCE_NAME="user=postgres host=/var/run/postgresql/ sslmode=disable"
ExecStart=/usr/local/bin/postgres_exporter
Restart=always

[Install]
WantedBy=multi-user.target
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Exporter & Metrics

<br>

Handlers

```
- name: restart_postgresql_exporter
  service:
    name: postgresql_exporter
    state: restarted
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Exporter & Metrics

<br>

Add scrape

```
- job_name: postgresql_exporter
  consul_sd_configs:
  - server: 'consul.service.xavki.consul:8500'
    services:
    - 'postgresql'
  relabel_configs:
  - source_labels: ['__meta_consul_address']
    regex:         '(.*)'
    target_label:  '__address__'
    replacement:   '${1}:9187'
  - source_labels: [__meta_consul_node]
    target_label: instance
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Exporter & Metrics

<br>

Add alerts

https://samber.github.io/awesome-prometheus-alerts/rules.html#postgresql-1


```
groups:
  - name: postgresql
    rules:
    - alert: Postgresql - replication change since 10m
      expr: rate(1-pg_replication_is_replica) != 0
      for: 10m
      labels:
        severity: critical
      annotations:
        summary: "{{ $labels.instance }} - replication chance"
        description: "check primary/secondary on the postgresql cluster"
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Exporter & Metrics

<br>

Add postgresql dashboard
