%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - node exporter

<br>

Step by step : 

  * create a new role

  * check if node exporter already install

  * create user & directories

  * install binary

  * install systemd service

  * handlers


-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - node exporter

<br>

Create some variables

```
node_exporter_version: "1.6.1"
node_exporter_bin: /usr/local/bin/node_exporter
node_exporter_user: node-exporter
node_exporter_group: "{{ node_exporter_user }}"
node_exporter_dir_conf: /etc/node_exporter
node_exporter_dir_data: /var/lib/node_exporter
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - node exporter

<br>

Check consul and consul version

```
- name: check if node exporter exist
  stat:
    path: "{{ node_exporter_bin }}"
  register: __check_node_exporter_present

- name: if node exporter exist get version
  shell: "cat /etc/systemd/system/node_exporter.service | grep Version | sed s/'.*Version '//g"
  register: __get_node_exporter_version
  when: __check_node_exporter_present.stat.exists == true
  changed_when: false
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - node exporter

<br>

Create user

```
- name: create node exporter user
  user:
    name: "{{ node_exporter_user }}"
    shell: /usr/sbin/nologin
    system: true
    create_home: false
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - node exporter

<br>

Create directories

```
- name: create node exporter config dir
  file:
    path: "{{ node_exporter_dir_conf }}"
    state: directory
    owner: "{{ node_exporter_user }}"
    group: "{{ node_exporter_group }}"
  loop:
    - "{{ node_exporter_dir_conf }}"
    - "{{ node_exporter_dir_data }}"
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - node exporter

<br>

Install binary

```
- name: download and unzip node exporter if not exist
  unarchive:
    src: "https://github.com/prometheus/node_exporter/releases/download/v{{ node_exporter_version }}/node_exporter-{{ node_exporter_version }}.linux-amd64.tar.gz"
    dest: /tmp/
    remote_src: yes
    validate_certs: false
  when: __check_node_exporter_present.stat.exists == false or not __get_node_exporter_version.stdout == node_exporter_version
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - node exporter

<br>

Install binary

```
- name: move the binary to the final destination
  copy:
    src: "/tmp/node_exporter-{{ node_exporter_version }}.linux-amd64/node_exporter"
    dest: "{{ node_exporter_bin }}"
    owner: "{{ node_exporter_user }}"
    group: "{{ node_exporter_group }}"
    mode: 0755
    remote_src: yes
  when: __check_node_exporter_present.stat.exists == false or not __get_node_exporter_version.stdout == node_exporter_version
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - node exporter

<br>

Clean

```
- name: clean
  file:
    path: /tmp/node_exporter-{{ node_exporter_version }}.linux-amd64/
    state: absent
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - node exporter

<br>

Install systemd service

```
- name: install service
  template:
    src: node_exporter.service.j2
    dest: /etc/systemd/system/node_exporter.service
    owner: root
    group: root
    mode: 0755
  notify: reload_daemon_and_restart_node_exporter
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - node exporter

<br>

Install start and flus handler

```
- meta: flush_handlers

- name: service always started
  systemd:
    name: node_exporter
    state: started
    enabled: yes
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - node exporter

<br>

Install handler

```
- name: reload_daemon_and_restart_node_exporter
  systemd:
    name: node_exporter
    state: restarted
    daemon_reload: yes
    enabled: yes
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - node exporter

<br>

Systemd template

```
[Unit]
Description=Node Exporter Version {{ node_exporter_version }}
After=network-online.target

[Service]
User={{ node_exporter_user }}
Group={{ node_exporter_group }}
Type=simple
ExecStart={{ node_exporter_bin }} --collector.textfile.directory {{ node_exporter_data }}

[Install]
WantedBy=multi-user.target
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - node exporter

<br>

Playbook

```
- name: consul services for all servers
  become: yes
  hosts: all
  serial: 40
  gather_facts: no
  roles:
    - consul/consul_services
  vars:
    consul_services:
      - { name: "node_exporter", type: "http", target: "http://127.0.0.1:9100", interval: "10s", port: 9100 }
```
