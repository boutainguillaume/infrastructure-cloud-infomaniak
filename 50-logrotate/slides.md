%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Logrotate & ansible

<br>

rotate : rotaiton number (by frequency : daily, monthly...)

missingok : don't stop on error

notifempty : no rotation for empty file

dateext : add date in journal archive

minsize : minimum size to rotate

copylog : copy file then truncate it after

copytruncate : copy file then truncate journal

create : permissions and user

sharedscripts : only once post rotate scripts per rotation

dateyesterday : use yesterday date in journal

postrotate : script to run after rotation

-----------------------------------------------------------------------------------------------------------                                       

# Logrotate & ansible

<br>

To close and reopen logs

```
kill -USR1 `pgrep traefik`
```

-----------------------------------------------------------------------------------------------------------                                       

# Logrotate & ansible

<br>

Variables

```
logrotate_user: root
logrotate_group: adm
logrotate_number: 15
logrotate_frequency: daily
```

-----------------------------------------------------------------------------------------------------------                                       

# Logrotate & ansible

<br>

Install logrotate

```
- name: Install logrotate
  apt:
    name: logrotate
    state: present
    update_cache: yes
    cache_valid_time: 3600
```

-----------------------------------------------------------------------------------------------------------                                       

# Logrotate & ansible

<br>

Standard configuration

```
- name: Configure logrotate
  template:
    src: logrotate.conf.j2
    dest: /etc/logrotate.conf
    mode: 0640
```

-----------------------------------------------------------------------------------------------------------                                       

# Logrotate & ansible

<br>

Specific configuration

```
- name: Generate logrotate conf files
  template:
    src: logrotated.conf.j2
    dest: "/etc/logrotate.d/{{ item.name }}"
    mode: "0644"
  loop: "{{ logrotate_log_files }}"
  when:
    - logrotate_log_files is defined
  loop_control:
    label: "{{ item.name }}"
```

-----------------------------------------------------------------------------------------------------------                                       

# Logrotate & ansible

<br>

Standard template

```
{{ ansible_managed | comment }}
# rotate frequency
{{ logrotate_frequency }}
su {{ logrotate_user }} {{ logrotate_group }}
# keep
rotate {{ logrotate_number }}
# create new (empty) log files after rotating old ones
create
# add dateext
dateext
compress
# packages drop log rotation information into this directory
include /etc/logrotate.d
```

-----------------------------------------------------------------------------------------------------------                                       

# Logrotate & ansible

<br>

Specific template

```
{{ ansible_managed | comment }}

{{ item.path }} {

    dateext
    compress
    missingok
    notifempty

{% if item.frequency is defined %}    {{ item.frequency }}{% else %}    daily{% endif %}

{% if item.delaycompress is defined and item.delaycompress %}    delaycompress{% endif %}

{% if item.number is defined %}    rotate {{ item.number }}{% else %}    rotate 15{% endif %}

{% if item.minsize is defined %}    minsize {{ item.minsize }}{% endif %}

{% if item.copylog is defined and item.copylog %}    copy{% endif %}

{% if item.copytruncate is defined and item.copytruncate %}    copytruncate{% endif %}

{% if item.create is defined and item.create %}    create{% if item.create_mode is defined %} {{ item.create_mode }}{% endif %}{% if item.create_user is defined %} {{ item.create_user }}{% endif %}{% if item.create_group is defined %} {{ item.create_group }}{% endif %}{% endif %}

{% if item.sharedscripts is defined and item.sharedscripts %}    sharedscripts{% endif %}

{% if item.dateformat is defined and item.dateformat %}    dateformat {{ item.dateformat }}{% endif %}

{% if item.dateyesterday is defined and item.dateyesterday %}    dateyesterday{% endif %}

{% if item.postrotate is defined %}
    postrotate
        {{ item.postrotate }}
    endscript {% else %}

    postrotate
        [ ! -x /usr/lib/rsyslog/rsyslog-rotate ] || /usr/lib/rsyslog/rsyslog-rotate
    endscript   
    {% endif %}

}
```

-----------------------------------------------------------------------------------------------------------                                       

# Logrotate & ansible

<br>

Example

```
  vars:
    logrotate_frequency: daily
    logrotate_keep: 7
    logrotate_compress: yes
    logrotate_log_files:
      - name: example
        path: "/var/log/example/*.log"
      - name: btmp
        path: /var/log/btmp
        missingok: yes
        frequency: monthly
        create: yes
        create_mode: "0660"
        create_user: root
        create_group: utmp
        dateext: yes
        dateformat: "-%Y-%m-%d"
```

-----------------------------------------------------------------------------------------------------------                                       

# Logrotate & ansible

<br>

For traefik

```
    logrotate_log_files:
      - name: traefik
        path: "/var/log/traefik/*.log"
```

Try to force 

```
logrotate --force  /etc/logrotate.conf
```