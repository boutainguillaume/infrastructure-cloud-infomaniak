%title: Infra Cloud Infomaniak
%author: xavki


██████╗  ██████╗ ███████╗████████╗ ██████╗ ██████╗ ███████╗███████╗ ██████╗ ██╗     
██╔══██╗██╔═══██╗██╔════╝╚══██╔══╝██╔════╝ ██╔══██╗██╔════╝██╔════╝██╔═══██╗██║     
██████╔╝██║   ██║███████╗   ██║   ██║  ███╗██████╔╝█████╗  ███████╗██║   ██║██║     
██╔═══╝ ██║   ██║╚════██║   ██║   ██║   ██║██╔══██╗██╔══╝  ╚════██║██║▄▄ ██║██║     
██║     ╚██████╔╝███████║   ██║   ╚██████╔╝██║  ██║███████╗███████║╚██████╔╝███████╗
╚═╝      ╚═════╝ ╚══════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚══════╝╚══════╝ ╚══▀▀═╝ ╚══════╝



-----------------------------------------------------------------------------------------------------------

# Postgresql : Backups

<br>

Changes : dashboards & PG alert

Some variables

```
backups_s3_key: ""
backups_secret_key: ""
backups_secret_password: ""
backups_cron_schedule: "0 1 * * *"
backups_path: "postgresql"
```

-----------------------------------------------------------------------------------------------------------

# Postgresql : Backups

<br>

Install restic

```
- name: install restic
  apt:
    name: restic
    state: present
    update_cache: yes
    cache_valid_time: 3600
```

-----------------------------------------------------------------------------------------------------------                     

# Postgresql : Backups

<br>

Install restic

```
- name: install directories
  file:
    path: "{{ item.dir }}"
    state: directory
    owner: root
    group: root
    mode: "{{ item.mode }}"
  loop:
    - { dir: "/root/.config/restic/", mode: "0750" }
    - { dir: "/opt/scripts/backups", mode: "0750" }
    - { dir: "/var/log/backups/", mode: "0755" }
    - { dir: "/var/lib/cron/", mode: "0755" }
```

-----------------------------------------------------------------------------------------------------------

# Postgresql : Backups

<br>

Add configuration

```
- name: add restic configuration file
  copy:
    content: |
      export AWS_DEFAULT_REGION="us-east-1"
      export RESTIC_REPOSITORY="s3:https://s3.pub1.infomaniak.cloud/{{ backups_path }}"
      export AWS_ACCESS_KEY_ID="{{ backups_s3_key }}"
      export AWS_SECRET_ACCESS_KEY="{{ backups_secret_key }}"
      export RESTIC_PASSWORD="{{ backups_secret_password }}"
    dest: "/root/.config/restic/restic_{{ backups_path }}.creds"
    owner: root
    group: root
    mode: 0700
```

-----------------------------------------------------------------------------------------------------------

# Postgresql : Backups

<br>

Install backkup script

```
- name: install the backup script
  template:
    src: "backup_{{ backups_path }}.sh.j2"
    dest: "/opt/scripts/backups/backup_{{ backups_path }}.sh"
    owner: root
    group: root
    mode: 0750
```

-----------------------------------------------------------------------------------------------------------

# Postgresql : Backups

<br>

Add a cron

```
- name: add a cron for backups
  template:
    src: backup_cron.j2
    dest: "/etc/cron.d/backup_{{ backups_path }}"
    owner: root
    group: root
    mode: 0750
```

-----------------------------------------------------------------------------------------------------------

# Postgresql : Backups

<br>

Check if init is needed

```
- name: check if restic already init
  shell: |
    source /root/.config/restic/restic_{{ backups_path }}.creds
    restic snapshots
  args:
    executable: /bin/bash
  register: __restic_init
  ignore_errors: true
```

-----------------------------------------------------------------------------------------------------------

# Postgresql : Backups

<br>

Init if needed

```
- name: init restic if needed
  shell: |
    source /root/.config/restic/restic_{{ backups_path }}.creds
    restic init
  args:
    executable: /bin/bash
  when: __restic_init.rc != 0
```

-----------------------------------------------------------------------------------------------------------

# Postgresql : Backups

<br>

Add cron template

```
## Edited by ansible
{{ backups_cron_schedule }} root touch /var/lib/cron/start_{{ backups_path }}_backup.txt && /opt/scripts/backups/backup_{{ backups_path }}.sh && touch /var/lib/cron/end_{{ backups_path }}_backup.txt
```

-----------------------------------------------------------------------------------------------------------

# Postgresql : Backups

<br>

Add the backup script


-----------------------------------------------------------------------------------------------------------

# Postgresql : Backups

<br>

Install the playbookk

```
- name: install our postgresql replication
  become: yes
  hosts: postgresql1
  roles:
    - databases/postgresql/postgresql_backups
    - monitoring/custom_metrics
```