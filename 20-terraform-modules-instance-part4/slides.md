%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 



-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance - Volumes

<br>

Purpose :

  * 1 to n instances

  * 0 to n volumes per instance

  * attribute a device name for each volume


-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance - Volumes

<br>


Locals 

    * an internal variable into a module

		* easy to use with for loop and generate complexe map

Example :

```
locals {
  device_names = ["/dev/sdb","/dev/sdc","/dev/sdd","/dev/sde","/dev/sdf","/dev/sdg"]
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance - Volumes

<br>

```
locals {
  instance_volume_map =  merge([
    
    for idxi, instance in openstack_compute_instance_v2.instance.*:
    {
      for idxv in range(var.instance_volumes_count):      
        "${instance.name}-volume-${idxv}" => {
            instance_name     = instance.name
            instance_id       = instance.id
            volume_name       = "${instance.name}-volume${idxv}"
            device            = local.device_names[idxv]
          }
    }
  ]...)
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance - Volumes

<br>

```
resource "openstack_blockstorage_volume_v2" "volumes" {
  for_each        = local.instance_volume_map
  name            = each.value.volume_name
  size            = var.instance_volumes_size
  volume_type     = var.instance_volumes_type
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance - Volumes

<br>

```
resource "openstack_compute_volume_attach_v2" "attached" {
  for_each      = local.instance_volume_map
  instance_id   = each.value.instance_id
  device        = each.value.device
  volume_id     = openstack_blockstorage_volume_v2.volumes[each.key].id
  # Prevent re-creation
  lifecycle {
    ignore_changes = [volume_id, instance_id, device]
  }
}
```
