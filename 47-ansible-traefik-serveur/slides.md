%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Traefik installation

<br>

Create an instance with fixed IP

Create a new role

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Traefik installation

<br>

Create some variables

```
traefik_version:  2.11.0
traefik_user: traefik
traefik_group: traefik
traefik_dir_conf: /etc/traefik
traefik_bin: /usr/local/bin
traefik_log_dir: /var/log/traefik/
traefik_tls_mail: xavki@gmail.com
traefik_tls_resolver: certs_gen
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Traefik installation

<br>

Create some variables

```
- name: check if traefik exists
  stat:
    path: "{{ traefik_bin }}"
  register: __check_traefik_present
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Traefik installation

<br>

Create some variables

```
- name: if traefik exists get version
  shell: "cat /etc/systemd/system/traefik.service | grep Version | sed s/'.*Version '//g"
  register: __get_traefik_version
  when: __check_traefik_present.stat.exists == true
  changed_when: false
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Traefik installation

<br>

Create some variables

```
- name: create traefik user
  user:
    name: "{{ traefik_user }}"
    shell: /usr/sbin/nologin
    system: true
    create_home: false
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Traefik installation

<br>

Create some variables

```
- name: create traefik config dir
  file:
    path: "{{ traefik_dir_conf }}"
    state: directory
    owner: "{{ traefik_user }}"
    group: "{{ traefik_group }}"
  loop:
    - "{{ traefik_dir_conf }}"
    - "{{ traefik_log_dir }}"
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Traefik installation

<br>

Create some variables

```
- name: create temp dir
  file:
    path: "/tmp/traefik_v{{ traefik_version }}_linux_amd64/"
    state: directory
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Traefik installation

<br>

Create some variables

```
- name: download and unzip traefik if not exist
  unarchive:
    src: "https://github.com/traefik/traefik/releases/download/v{{ traefik_version }}/traefik_v{{ traefik_version }}_linux_amd64.tar.gz"
    dest: /tmp/traefik_v{{ traefik_version }}_linux_amd64/
    remote_src: yes
    validate_certs: false
  when: __check_traefik_present.stat.exists == false or not __get_traefik_version.stdout == traefik_version
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Traefik installation

<br>

Create some variables

```
- name: move the binary to the final destination
  copy:
    src: "/tmp/traefik_v{{ traefik_version }}_linux_amd64/traefik"
    dest: "{{ traefik_bin }}"
    owner: "{{ traefik_user }}"
    group: "{{ traefik_group }}"
    mode: 0750
    remote_src: yes
  when: __check_traefik_present.stat.exists == false or not __get_traefik_version.stdout == traefik_version
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Traefik installation

<br>

Create some variables

```
- name: clean
  file:
    path: /tmp/traefik_v{{ traefik_version }}_linux_amd64/
    state: absent
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Traefik installation

<br>

Create some variables

```
- name: install service
  template:
    src: traefik.service.j2
    dest: /etc/systemd/system/traefik.service
    owner: root
    group: root
    mode: 0750
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Traefik installation

<br>

Create some variables

```
- name: add configuration
  template:
    src: {{ item }}.j2
    dest: "{{ traefik_dir_conf }}/{{ item }}"
    owner: "{{ traefik_user }}"
    group: "{{ traefik_group }}"
    mode: 0750
  loop:
    - "traefik.toml"
    - "dynamic.toml"
  notify: restart_traefik
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Traefik installation

<br>

Create some variables

```
- name: service always started
  systemd:
    name: traefik
    state: started
    enabled: true
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Traefik installation

<br>

Create some variables

```
[log]
  level = "INFO"
  filePath = "/var/log/traefik/traefik.log"
  format = "json"

[accessLog]
  filePath =  "/var/log/traefik/access-traefik.log"
  format = "json" 
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Traefik installation

<br>

Create some variables

```
[file]
watch = true

[api]
  dashboard = true
  insecure = false
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Traefik installation

<br>

Create some variables

```
[entryPoints]
  [entryPoints.http]
    address = ":80"
    [entryPoints.http.http.redirections]
      [entryPoints.http.http.redirections.entryPoint]
        to = "https"
        scheme = "https"
    [entryPoints.http.http2]
      maxConcurrentStreams = 250
  [entryPoints.https]
    address = ":443"
    [entryPoints.https.http2]
      maxConcurrentStreams = 250
  [entryPoints.api]
    address = ":8080"
  [entryPoints.metrics]
    address = ":9200"
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Traefik installation

<br>

Create some variables

```
[certificatesResolvers]
  [certificatesResolvers.{{ traefik_tls_resolver }}]
    [certificatesResolvers.{{ traefik_tls_resolver }}.acme]
      email = "{{ traefik_tls_mail }}"
      caServer = "https://acme-v02.api.letsencrypt.org/directory"
      storage = "acme.json"
      keyType = "EC384"
        [certificatesResolvers.{{ traefik_tls_resolver }}.acme.httpChallenge]
          entryPoint = "http"
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Traefik installation

<br>

Create some variables

```
[providers.file]
  directory = "{{ traefik_dir_conf }}"             
  watch = true

[metrics]
  [metrics.prometheus]
    entryPoint = "metrics"
    buckets = [0.1,0.3,1.2,5.0]
    addEntryPointsLabels = true
    addRoutersLabels = true
    addServicesLabels = true
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Traefik installation

<br>

Create some variables

```
[http]
  [http.routers]
    [http.routers.api]
      rule = "PathPrefix(`/api`, `/dashboard`)"
      entryPoints = ["api"]
      service = "api@internal"
      middlewares = ["auth","allow_ip"]
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Traefik installation

<br>

Create some variables

```
  [http.middlewares]
    [http.middlewares.auth.basicAuth]
      users = [
      "monnomestpersonne:$apr1$rnqfuhkt$u7ubv2bBKQYNscWTw2xy./"
      ]
    [http.middlewares.to_https.redirectScheme]
      scheme = "https"
      permanent = true
    [http.middlewares.allow_ip.ipAllowList]
      sourceRange = ["127.0.0.1/32", "10.0.1.0/24"]
    [http.middlewares.secure_headers.headers]
      framedeny = true
      browserxssfilter = true
      contentTypeNosniff = true
      stsIncludeSubdomains = true
      stsPreload = true
      stsSeconds = 31536000
      forceStsHeader = true
      referrerPolicy = "same-origin"
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Traefik installation

<br>

Create some variables

```
[tls]
  [tls.options]
    [tls.options.default]
      minVersion = "VersionTLS13"
      sniStrict = true
      cipherSuites = [
        "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384",
        "TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305",
      ]
      curvePreferences = ["CurveP521","CurveP384"]
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Traefik installation

<br>

Create some variables

```
[Unit]
Description=traefik proxy
After=network-online.target
Wants=network-online.target systemd-networkd-wait-online.service

[Service]
Type=simple
User={{ traefik_user }}
Group={{ traefik_group }}
Restart=on-failure
ExecStart=/usr/local/bin/traefik --configfile=/etc/traefik/traefik.toml

[Install]
WantedBy=multi-user.target
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Traefik installation

<br>

Create some variables

```
- name: restart_traefik
  service:
    name: traefik
    state: restarted
    enabled: true
    daemon_reload: yes
```