%title: Infra Cloud Infomaniak
%author: xavki


██████╗  ██████╗ ███████╗████████╗ ██████╗ ██████╗ ███████╗███████╗ ██████╗ ██╗     
██╔══██╗██╔═══██╗██╔════╝╚══██╔══╝██╔════╝ ██╔══██╗██╔════╝██╔════╝██╔═══██╗██║     
██████╔╝██║   ██║███████╗   ██║   ██║  ███╗██████╔╝█████╗  ███████╗██║   ██║██║     
██╔═══╝ ██║   ██║╚════██║   ██║   ██║   ██║██╔══██╗██╔══╝  ╚════██║██║▄▄ ██║██║     
██║     ╚██████╔╝███████║   ██║   ╚██████╔╝██║  ██║███████╗███████║╚██████╔╝███████╗
╚═╝      ╚═════╝ ╚══════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚══════╝╚══════╝ ╚══▀▀═╝ ╚══════╝



-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : User, Schema and Database management

<br>

Our target ?

```
- name: postgresql
  become: yes
  hosts: postgresql1
  roles:
    - databases/postgresql_manage
  vars:
    postgresql_manage:
      users:
        - { name: "xavki_mattermost", password: "password" }
      db_name:
        - mattermost
      privileges_database:
        - { db: "mattermost" , type: "database", user: "xavki_mattermost", permissions: "ALL" }
      privileges_schema:
        - { schemas_name: "public", owner: "xavki_mattermost", db: "mattermost" }
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : User, Schema and Database management

<br>

Add to pg_hba

```
    postgresql_pghba_entries:
      - {host: 'host', database: 'all', user: 'all', address: '10.0.1.0/24', method: 'md5'}
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : User, Schema and Database management

<br>

Tasks

```
- name: manage users & databases
  include_tasks: manage_users_databases.yml
  when: postgresql_manage
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : User, Schema and Database management

<br>

Tasks

```
- name: Ensure psycopg2 installed
  apt:
    name: python3-psycopg2
    state: present
    update_cache: yes
    cache_valid_time: 3600
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : User, Schema and Database management

<br>

Create user

```
- name: "Create user {{ item.name }}"
  become_user: postgres
  postgresql_user:
    name: "{{ item.name }}"
    password: "{{ item.password }}"
    encrypted: true
  loop: "{{ postgresql_manage.users }}"
  when: postgresql_manage.users
  no_log: true
```

Warning : no_log !!!

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : User, Schema and Database management

<br>

Create database

```
- name: "Create database {{ item  }}"
  become_user: postgres
  postgresql_db:
    name: "{{ item }}"
    owner: "postgres"
  loop: "{{ postgresql_manage.db_name }}"
  when: postgresql_manage.db_name
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : User, Schema and Database management

<br>

Add some privileges

```
- name: "Privileges database {{ item.user }} - {{ item.permissions }}"
  become_user: postgres
  postgresql_privs:
    db: "{{ item.db }}"
    type: "{{ item.type }}"
    privs: "{{ item.permissions }}"
    roles: "{{ item.user }}"
  loop: "{{ postgresql_manage.privileges_database }}"
  when: postgresql_manage.privileges_database
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : User, Schema and Database management

<br>

Create schema

```
- name: "Create schema {{ item.schemas_name }}"
  become_user: postgres
  postgresql_schema:
    name: "{{ item.schemas_name }}"
    owner: "{{ item.owner }}"
    database: "{{ item.db }}"
  loop: "{{ postgresql_manage.privileges_schema }}"
  when: postgresql_manage.privileges_schema is defined
```

