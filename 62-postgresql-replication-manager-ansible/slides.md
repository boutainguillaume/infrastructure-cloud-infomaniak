%title: Infra Cloud Infomaniak
%author: xavki


██████╗  ██████╗ ███████╗████████╗ ██████╗ ██████╗ ███████╗███████╗ ██████╗ ██╗     
██╔══██╗██╔═══██╗██╔════╝╚══██╔══╝██╔════╝ ██╔══██╗██╔════╝██╔════╝██╔═══██╗██║     
██████╔╝██║   ██║███████╗   ██║   ██║  ███╗██████╔╝█████╗  ███████╗██║   ██║██║     
██╔═══╝ ██║   ██║╚════██║   ██║   ██║   ██║██╔══██╗██╔══╝  ╚════██║██║▄▄ ██║██║     
██║     ╚██████╔╝███████║   ██║   ╚██████╔╝██║  ██║███████╗███████║╚██████╔╝███████╗
╚═╝      ╚═════╝ ╚══════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚══════╝╚══════╝ ╚══▀▀═╝ ╚══════╝



-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part 1


<br>

Purpose :

  * create a new role for postgresql replication

  * focus on postgresql and replication manager (failover management in a next video)

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part 1


<br>

The playbook - postgresql simple

```
- name: install our postgresql cluster
  become: yes
  hosts: meta-app_postgresql
  roles:
    - volumes
    - databases/postgresql/postgresql_simple
  vars:
    volumes_disks:
      - {disk: '/dev/sdb',path: '/data', owner: "root"}
    postgresql_simple_data_dir: /data/postgresql
    postgresql_simple_pghba_enabled: false
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part 1


<br>

The playbook - postgresql replication

```
- name: install our postgresql replication
  become: yes
  hosts: postgresql1 #,postgresql2
  serial: 1
  roles:
    - databases/postgresql/postgresql_replication
  vars:
    postgresql_replication_data_dir: "/data/postgresql"
    postgresql_replication_primary: "postgresql1"
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part 1


<br>

Variables - postgresql replication

```
postgresql_replication_version_major: 16
postgresql_replication_group: "meta-app_postgresql"
postgresql_replication_repmgr_password: "password"
postgresql_replication_pattern: "postgresql"
postgresql_replication_data_dir: ""
#postgresql_replication_pghba_entries
```

for simple postgresql - condition the pg_hba (defaults and when)

```
when: postgresql_simple_pghba_enabled
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part 1


<br>

main.yml - Important need to change our internal dns
  (to prevent a consul restart >> consul dns loss = failover no !!)

```
- name: Update the /etc/hosts
  lineinfile:
    dest: "/etc/hosts"
    regexp: ".*\t{{ item }}\t{{ item }}"
    line: "{{ hostvars[item]['ansible_host'] }}\t{{ item }}\t{{ item }}"
    state: present
  when: ansible_hostname != item
  loop: "{{ groups[postgresql_replication_group] }}"
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part 1


<br>

main.yml - Install repmgr 

```
- name: install repmgr
  ansible.builtin.apt:
    name: repmgr,python3-psycopg2
    state: present
    update_cache: true
```

Note : prerequisite postgresql and repository

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part 1


<br>

main.yml - Check if the repmgr user exists

```
- name: create repmgr user
  community.postgresql.postgresql_user:
    name: repmgr
    password: '{{ postgresql_replication_repmgr_password }}'
    role_attr_flags: REPLICATION,CREATEDB,CREATEROLE,SUPERUSER
  become_user: postgres
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part 1


<br>

main.yml - Create repmgr database

```
- name: "create database repmgr"
  shell: createdb repmgr -O repmgr
  when: __repmgr_exist.stdout == "0"  and ansible_hostname == postgresql_replication_primary 
  become_user: postgres
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part 1


<br>

main.yml - Push a new pg_hba file

```
- name: add pg_hba.conf
  template:
    src: pg_hba.conf.j2
    dest: "/etc/postgresql/{{ postgresql_replication_version_major }}/main/pg_hba.conf"
    mode: 0640
    owner: postgres
    group: postgres
  notify: reload_postgresql
```


-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part 1


<br>

main.yml - Push a new pg_pass file

```
- name: add pgpass for authentication
  template:
    src: pgpass.j2
    dest: "/var/lib/postgresql/.pgpass"
    mode: 0600
    owner: postgres
    group: postgres
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part 1


<br>

main.yml - Push the replication.conf template

```
- name: add replication.conf for replication settings
  template:
    src: replication.conf.j2
    dest: "/etc/postgresql/{{ postgresql_replication_version_major }}/main/conf.d/replication.conf"
    mode: 0640
    owner: postgres
    group: postgres
  notify: restart_postgresql

- name: "Flush handlers"
  meta: flush_handlers
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part 1


<br>

main.yml - Push the repmgrd default options configuration

```
- name: add /etc/default/repmgr settings
  template:
    src: repmgrd.j2
    dest: /etc/default/repmgrd
    mode: 0640
    owner: postgres
    group: postgres
  notify: restart_repmgr
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part 1


<br>

main.yml - Push the repmgr configuration

```
- name: "add repmgr.conf"
  template:
    src: repmgr.conf.j2
    dest: /etc/repmgr.conf
    mode: 0640
    owner: postgres
    group: postgres
  notify: restart_repmgr
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part 1


<br>

main.yml - Start repmgr

```
- name: "Flush handlers"
  meta: flush_handlers

- name: start repmgr if not already start
  service:
    name: repmgrd
    state: started
    enabled: yes
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part 1


<br>

main.yml - Check repmgr status

```
- name: check repmgr status
  shell: "repmgr cluster show"
  become_user: postgres
  changed_when: False
  ignore_errors: True
  register: __test_repmgr
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part 1


<br>

main.yml - add a subtask file

```
include_tasks: primary.yml
```

master.yml - Register the master node if not already registered

```
- name: register primary
  shell: repmgr primary register --force
  become_user: postgres
  when: ansible_hostname == postgresql_replication_primary and ansible_hostname not in __test_repmgr.stdout 
```

-----------------------------------------------------------------------------------------------------------                                    

# Postgresql : Ansible && Replication Manager - part 1


<br>

master.yml - Flush handlers

```
- name: "Flush handlers"
  meta: flush_handlers
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part 1


<br>

Handlers

```
- name: restart_repmgr
  systemd:
    name: repmgrd
    state: restarted
    enabled: yes
    daemon_reload: yes
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part 1


<br>

Handlers

```
- name: restart_postgresql
  service:
    name: postgresql
    state: restarted

- name: reload_postgresql
  service:
    name: postgresql
    state: reloaded
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Ansible && Replication Manager - part 1


<br>

Add templates

