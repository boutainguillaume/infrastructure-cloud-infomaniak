%title: Infra Cloud Infomaniak
%author: xavki


██╗   ██╗███████╗ ██████╗████████╗ ██████╗ ██████╗ 
██║   ██║██╔════╝██╔════╝╚══██╔══╝██╔═══██╗██╔══██╗
██║   ██║█████╗  ██║        ██║   ██║   ██║██████╔╝
╚██╗ ██╔╝██╔══╝  ██║        ██║   ██║   ██║██╔══██╗
 ╚████╔╝ ███████╗╚██████╗   ██║   ╚██████╔╝██║  ██║
  ╚═══╝  ╚══════╝ ╚═════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝


-----------------------------------------------------------------------------------------------------------                                       

# Vector : introduction

<br>

What is it ?

    * collect / transform / route

    * = streamer

    * like : fluentbit, fluentd, filebeat, logstash, benthos

    * focus on observability (don't keep it in your mind)

    * created by datadog


-----------------------------------------------------------------------------------------------------------                                       

# Vector : introduction

<br>

Github : https://github.com/vectordotdev/vector
Official website : https://vector.dev/

Language : Rust


-----------------------------------------------------------------------------------------------------------                                       

# Vector : introduction

<br>

Different modes :

    * agent as daemon : all data/logs about a single host (like daemonset :) )

    * agent as sidecar : each service/apps have one vector as sidecar (data owner)

    * aggregator : central role to process data on dedicated host or specific usage


-----------------------------------------------------------------------------------------------------------                                       

# Vector : introduction

<br>

Events :

    * log events : capture raw data to convert it into log formated data

    * metric events : capture raw data to convert it into metrics !!! (counter, gauge...)

-----------------------------------------------------------------------------------------------------------                                       

# Vector : introduction

<br>

Components :

    * ingest, transform and route events

    * 3 types of components : 

      Sources > Transform > Sinks

-----------------------------------------------------------------------------------------------------------                                       

# Vector : introduction

<br>

Pipeline :

    * directed acyclic graph of components

    * nodes (components) are interconnected to process data

    * define in setting file : yaml, toml, json

    * support hot reload

-----------------------------------------------------------------------------------------------------------                                       

# Vector : introduction

<br>

```
curl --proto '=https' --tlsv1.2 -sSfL https://sh.vector.dev | bash
```

-----------------------------------------------------------------------------------------------------------                                       

# Vector : introduction

<br>

```
sources:
  generate_syslog:
    type:   "demo_logs"
    format: "syslog"
    count:  100
```

-----------------------------------------------------------------------------------------------------------                                       

# Vector : introduction

<br>

```
transforms:
  remap_syslog:
    inputs:
      - "generate_syslog"
    type:   "remap"
    source: |
            structured = parse_syslog!(.message)
            . = merge(., structured)            
```

-----------------------------------------------------------------------------------------------------------                                       

# Vector : introduction

<br>

```
sinks:
  emit_syslog:
    inputs:
      - "remap_syslog"
    type: "console"
    encoding:
      codec: "json"
```
