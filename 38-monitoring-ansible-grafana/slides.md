%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Grafana

<br>

Grafana

  * visualization

  * dashboards : large community

  * many datasources

  * covers all observability : metrics, logs and traces

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Grafana

<br>

Add some variables (be careful we have a secret)

```
grafana_admin_user: "xavki"
grafana_admin_password: "password"
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Grafana

<br>

Ensure that we have prerequisites (in base role ?)

```
- name: install gpg
  apt:
    name: gnupg,software-properties-common
    state: present
    update_cache: yes
    cache_valid_time: 3600
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Grafana

<br>

Add the apt key and repository

```
- name: add gpg hey
  apt_key:
    url: "https://packages.grafana.com/gpg.key"
    validate_certs: no

- name: add repository
  apt_repository:
    repo: "deb https://packages.grafana.com/oss/deb stable main"
    state: present
    validate_certs: no
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Grafana

<br>

Install grafana

```
- name: install grafana
  apt:
    name: grafana
    state: present
    update_cache: yes
    cache_valid_time: 3600
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Grafana

<br>

Change the default password

```
- name: change admin user
  lineinfile:
    path: /etc/grafana/grafana.ini
    regexp: "{{ item.before }}"
    line: "{{ item.after }}"
  with_items:
  - { before: "^;admin_user = admin", after: "admin_user = {{ grafana_admin_user }}"}
  - { before: "^;admin_password = admin", after: "admin_password = {{ grafana_admin_password }}"}
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Grafana

<br>

Start grafana

```
- name: start service grafana-server
  systemd:
    name: grafana-server
    state: started
    enabled: yes
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Grafana

<br>

Check that grafana is started with a retry

```
- name: wait for service up
  uri:
    url: "http://127.0.0.1:3000"
    status_code: 200
  register: __result
  until: __result.status == 200
  retries: 120
  delay: 1
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Grafana

<br>

Change the default password

```
- name: change admin password for grafana gui
  shell : "grafana-cli admin reset-admin-password {{ grafana_admin_password }}"
  register: __command_admin
```