%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Traefik - dashboard grafana & alerts

<br>

Add traefik metrics


```
- job_name: traefik_metrics
  consul_sd_configs:
  - server: 'consul.service.xavki.consul:8500'
    services:
    - 'traefik_metrics'
  relabel_configs:
  - source_labels: ['__meta_consul_address']
    regex:         '(.*)'
    target_label:  '__address__'
    replacement:   '${1}:9200'
  - source_labels: [__meta_consul_node]
    target_label: instance
```

-----------------------------------------------------------------------------------------------------------                                       

# Traefik - dashboard grafana & alerts

<br>

Grafana dashboard

https://grafana.com/grafana/dashboards/17346-traefik-official-standalone-dashboard/

```
grafana_dashboards:
- { label: "traefik", json: "https://raw.githubusercontent.com/traefik/traefik/master/contrib/grafana/traefik.json", mode: "web" }
```

-----------------------------------------------------------------------------------------------------------                                       

# Traefik - dashboard grafana & alerts

<br>

Alerting on vmalert


https://samber.github.io/awesome-prometheus-alerts/rules#traefik