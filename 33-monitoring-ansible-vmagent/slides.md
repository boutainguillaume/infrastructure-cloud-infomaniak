%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VMagent & ansible

<br>

VMagent

  * doc : https://docs.victoriametrics.com/vmagent/

  * scrape our node exporter

  * use service discovery... consul

  * features : authentication (bearer-token, cardinality limiter, relabels...)

  * gui port 8429


-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VMagent & ansible

<br>

Step by step :

  * create a new instance (terraform module)

  * initialize a new role

  * check if the binary exists

  * install it if needed

  * templatize the configuration file

  * add a systemd service

  * start it !!

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VMagent & ansible

<br>

Start with some variables

```
module "monitoring" {
  source                      = "../modules/instance"
  instance_count              = 1
  instance_name               = "monitoring"
  instance_key_pair           = "default_key"
  instance_security_groups    = ["consul", "ssh-internal", "default"]
  instance_network_internal   = var.network_internal_dev
  instance_ssh_key = var.ssh_public_key_default_user
  public_floating_ip = false
  instance_default_user = var.default_user
  instance_volumes_count = 1  
  metadatas                   = {
    environment          = "dev",
    app         = "monitoring"
  }
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VMagent & ansible

<br>

Create a new security group and push its on all instances (openvpn part & consul)

```
resource "openstack_networking_secgroup_v2" "node_exporter" {
  name        = "node_exporter"
  description = "node_exporter"
}

resource "openstack_networking_secgroup_rule_v2" "secgroup_node_exporter" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 9100
  port_range_max    = 9100
  remote_ip_prefix  = var.network_subnet_cidr
  security_group_id = openstack_networking_secgroup_v2.node_exporter.id
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VMagent & ansible

<br>

Start with some variables

```
vmagent_dir_disk_data: ""
vmagent_version: 1.91.3
vmagent_dir_data: "{{ vmagent_dir_disk_data }}/var/lib/vmagent"
vmagent_etc_config: "/etc/vmagent"
vmagent_remotewrite: "127.0.0.1:8428"
vmagent_scrape_interval: 10s
vmagent_env: dev
vmagent_var_config:
  global:
    scrape_interval: "{{ vmagent_scrape_interval }}"
    external_labels:
      env: '{{ vmagent_env }}'
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VMagent & ansible

<br>

Check if cmagent already installed

```
- name: check if vmagent exists
  stat:
    path: /usr/local/bin/vmagent-prod
  register: __vmagent_exists

- name: if vmagent exists get version
  shell: "cat /etc/systemd/system/vmagent.service | grep Version | sed s/'.*Version '//g"
  register: __get_vmagent_version
  when: __vmagent_exists.stat.exists == true
  changed_when: false
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VMagent & ansible

<br>

Create directories

```
- name: create directory for vmagent data 
  file:
    path: "{{ item }}"
    state: directory
    mode: 0750
    recurse: yes
  loop:
    - "{{ vmagent_etc_config }}"
    - "{{ vmagent_data_dir }}"
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VMagent & ansible

<br>

Download and install vmutils tools (vmagent and others)

```
- name: download vmstack
  unarchive:
    src: https://github.com/VictoriaMetrics/VictoriaMetrics/releases/download/v{{ vmagent_version }}/vmutils-linux-amd64-v{{ vmagent_version }}.tar.gz
    dest: /usr/local/bin
    remote_src: yes
  when: __vmagent_exists.stat.exists == False or not __get_vmagent_version.stdout == vmagent_version
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VMagent & ansible

<br>

Add systemd service

```
- name: vmagent configuration file
  template:
    src: "vmagent.service.j2"
    dest: "/etc/systemd/system/vmagent.service"         
    mode: 0750
  notify: "restart_vmagent"
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VMagent & ansible

<br>

```
- name: vmagent configuration file
  template:
    src: vmagent.yml.j2
    dest: "{{ vmagent_etc_config }}/vmagent.yml"           
    mode: 0750
  notify: restart_vmagent
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VMagent & ansible

<br>

Start and flush handlers

```
- meta: flush_handlers

- name: start vmagent
  systemd:
    name: vmagent
    state: started
    enabled: yes
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VMagent & ansible

<br>

Add the handler

```
- name: restart_vmagent
  systemd:
    name: vmagent
    state: restarted
    enabled: yes
    daemon_reload: yes
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VMagent & ansible

<br>

```
[Unit]
Description=Description=VictoriaAgent service Version {{ vmagent_version }}
After=network.target

[Service]
Type=simple
ExecStart=/usr/local/bin/vmagent-prod \
       -promscrape.config={{vmagent_etc_config}}/vmagent.yml \
       -remoteWrite.tmpDataPath={{ vmagent_data_dir }} \
       -remoteWrite.url=http://{{ vmagent_remotewrite }}/api/v1/write \
       -promscrape.config.strictParse=false

SyslogIdentifier=vmagent
Restart=always

[Install]
WantedBy=multi-user.target
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VMagent & ansible

<br>

```
#jinja2: lstrip_blocks: "True"
{{ vmagent_var_config | to_nice_yaml(indent=2) }}

scrape_configs:

- job_name: node_exporter
  consul_sd_configs:
  - server: 'consul.service.xavki.consul:8500'
    services:
    - 'node_exporter'
  relabel_configs:
  - source_labels: ['__meta_consul_address']
    regex:         '(.*)'
    target_label:  '__address__'
    replacement:   '${1}:9100'
  - source_labels: [__meta_consul_node]
    target_label: instance
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VMagent & ansible

<br>

Add the playbook

```
- name: monitoring
  become: yes
  hosts: meta-app_monitoring
  roles:
    - volumes
    - vmagent
  vars:
    vmagent_dir_disk_data: "/data"
    volumes_disks:
      - { disk: '/dev/sdb', path: '{{ vmagent_dir_disk_data }}' }
```