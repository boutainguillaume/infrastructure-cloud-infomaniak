%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 



-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance - Count & Condition on IPs

We have a problem ??

<br>

Purpose :

		* Create one or multiple instance for a same group

		* Create fixed or random internal ip

		* Create fixed or random external ip


-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance - Count & Condition on IPs


<br>

New variable in instance module

```
variable "instance_count" {
  type = number
  default = 1
}
variable "public_floating_ip" {
  type = bool
  default = false
}
variable "public_floating_ip_fixed" {
  type = string
  default = ""
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance - Count & Condition on IPs


<br>

Adapt compute resouce to iterate over a counter

```
resource "openstack_compute_instance_v2" "instance" {
  count           = var.instance_count
  name            = "${var.instance_name}${count.index + 1}" #
...
  network {
    name = var.instance_network_name
    fixed_ip_v4 = var.instance_internal_fixed_ip == "" ? "" : "${var.instance_internal_fixed_ip}${count.index + 1}"#
  }
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance - Count & Condition on IPs

<br>

Check if public ip needed to get network data

```
data "openstack_networking_subnet_ids_v2" "ext_subnets" {
  count = var.public_floating_ip ? 1 : 0
  network_id = var.instance_network_external_id
}
```

Note : o = no data source (same for resources)

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance - Count & Condition on IPs

<br>

Check if random floating ip

```
resource "openstack_networking_floatingip_v2" "floatip_1" {
  count = var.public_floating_ip  && var.public_floating_ip_fixed == "" ? 1 : 0
  pool       = data.openstack_networking_network_v2.ext_network[count.index].name
  subnet_ids = data.openstack_networking_subnet_ids_v2.ext_subnets[count.index].ids
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance - Count & Condition on IPs

<br>

Associate random or fixed floating ip

```
resource "openstack_compute_floatingip_associate_v2" "fip_assoc" {
  count = var.public_floating_ip ? 1 : 0
  floating_ip = var.public_floating_ip_fixed != "" ? var.public_floating_ip_fixed : openstack_networking_floatingip_v2.floatip_1[count.index].address
  instance_id = openstack_compute_instance_v2.instance[count.index].id
}
```
