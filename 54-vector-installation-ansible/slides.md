%title: Infra Cloud Infomaniak
%author: xavki


██╗   ██╗███████╗ ██████╗████████╗ ██████╗ ██████╗ 
██║   ██║██╔════╝██╔════╝╚══██╔══╝██╔═══██╗██╔══██╗
██║   ██║█████╗  ██║        ██║   ██║   ██║██████╔╝
╚██╗ ██╔╝██╔══╝  ██║        ██║   ██║   ██║██╔══██╗
 ╚████╔╝ ███████╗╚██████╗   ██║   ╚██████╔╝██║  ██║
  ╚═══╝  ╚══════╝ ╚═════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝


-----------------------------------------------------------------------------------------------------------                                       

# Vector : installation avec ansible

<br>

Purposes :

    * add loki as datasource to grafana

    * create a role for vector

    * install user and directories

    * install vector binary

    * install config files


-----------------------------------------------------------------------------------------------------------                                       

# Vector : installation avec ansible

<br>

Add loki datasource

```
grafana_datasources:
  - {name: "VictoriaMetrics", type: "prometheus", address: "vmetrics.service.consul:8428"}
  - {name: "Loki", type: "loki", address: "loki.service.consul:3100"} 
```

```
ds_url: "http://{{ item.address }}"
```

-----------------------------------------------------------------------------------------------------------                                       

# Vector : installation avec ansible

<br>

Some variables

```
vector_dir_data: "/var/lib/vector"
vector_dir_config: "/etc/vector"
vector_version: 0.37.0
vector_binary_path: "/usr/local/bin/"
vector_loki_endpoint: loki.service.xavki.consul:3100
vector_list: []
```

-----------------------------------------------------------------------------------------------------------                                       

# Vector : installation avec ansible

<br>

Create group and user

```
- name: create group vector
  ansible.builtin.group:
    name: vector
    system: yes
    state: present

- name: create user vector
  ansible.builtin.user:
    name: vector
    system: yes
    shell: /sbin/nologin
    state: present
    groups: adm
    append: true
```

-----------------------------------------------------------------------------------------------------------                                       

# Vector : installation avec ansible

<br>

Create directories

```
- name: create directory for vector agent data
  ansible.builtin.file:
    path: "{{ item }}"
    state: directory
    mode: 0750
    owner: vector
    group: vector
    recurse: yes
  loop:
  - "{{ vector_dir_data }}"
  - "{{ vector_dir_config }}"
```

-----------------------------------------------------------------------------------------------------------                                       

# Vector : installation avec ansible

<br>

Check if exists & version

```
- name: check if vector exists
  ansible.builtin.stat:
    path: "{{ vector_binary_path }}/vector"
  register: __vector_exists

- name: if vector exists get version
  ansible.builtin.shell: "cat /etc/systemd/system/vector.service | grep Version | sed s/'.*Version '//g"
  register: __vector_get_version
  when: __vector_exists.stat.exists == true
  changed_when: false
```

-----------------------------------------------------------------------------------------------------------                                       

# Vector : installation avec ansible

<br>

Install binary

```
- name: download vector
  ansible.builtin.unarchive:
    src: https://packages.timber.io/vector/{{ vector_version }}/vector-{{ vector_version }}-x86_64-unknown-linux-musl.tar.gz        
    dest: "{{ vector_binary_path }}"
    extra_opts:
    - ./vector-x86_64-unknown-linux-musl/bin/vector
    - --strip-components=3
    remote_src: yes
    mode: 0750
    owner: vector
    group: vector
  when: not __vector_exists.stat.exists or not __vector_get_version.stdout == vector_version
```

-----------------------------------------------------------------------------------------------------------                                       

# Vector : installation avec ansible

<br>

Create systemd service

```
- name: add systemd service for vector
  template:
    src: vector.service.j2
    dest: "/etc/systemd/system/vector.service"
    owner: root
    group: root
    mode: 0750
  notify: restart_vector
```

-----------------------------------------------------------------------------------------------------------                                       

# Vector : installation avec ansible

<br>

Add pipeline templates

```
- name: add vector pipelines
  template:
    src: "{{ item }}"
    dest: "/etc/vector/{{ item | basename }}"
    owner: vector
    group: vector
    mode: 0750
  with_fileglob:
    - templates/configs/*.yaml
  when: item | basename | replace('.yaml','') in vector_list
  notify: reload_vector
```

-----------------------------------------------------------------------------------------------------------                                       

# Vector : installation avec ansible

<br>

Ensure started

```
- name: start vector
  service:
    name: vector
    state: started
    enabled: yes
```

-----------------------------------------------------------------------------------------------------------                                       

# Vector : installation avec ansible

<br>

Add handlers

```
- name: restart_vector
  service:
    name: vector
    state: restarted
    enabled: yes
    daemon-reload: yes

- name: reload_vector
  service:
    name: vector
    state: reloaded 
```

-----------------------------------------------------------------------------------------------------------                                       

# Vector : installation avec ansible

<br>

Add systemd service template

```
[Unit]
Description=Vector Version {{ vector_version }}
Documentation=https://vector.dev
After=network-online.target
Requires=network-online.target
[Service]
User=vector
Group=vector
ExecStartPre=/usr/local/bin/vector validate --config-yaml /etc/vector/*.yaml
ExecStart=/usr/local/bin/vector --config /etc/vector/*.yaml
ExecReload=/usr/local/bin/vector validate --config-yaml /etc/vector/*.yaml
ExecReload=/bin/kill -HUP $MAINPID
Restart=always
AmbientCapabilities=CAP_NET_BIND_SERVICE
EnvironmentFile=-/etc/default/vector
# Since systemd 229, should be in [Unit] but in order to support systemd <229,
# it is also supported to have it here.
StartLimitInterval=10
StartLimitBurst=5
[Install]
WantedBy=multi-user.target
```

-----------------------------------------------------------------------------------------------------------                                       

# Vector : installation avec ansible

<br>

Add pipelines - journald

```
data_dir: "/var/lib/vector"

# Ingest
sources:
  journald:
    type: journald

sinks:
  loki_journald:
    type: loki
    labels:
      instance: "{{ ansible_hostname }}"
      type: "journald"
    inputs:
      - journald
    endpoint: http://{{ vector_loki_endpoint }}
    encoding:
      codec: "json"
```

-----------------------------------------------------------------------------------------------------------                                       

# Vector : installation avec ansible

<br>

Add pipelines - traefik

```
data_dir: "/var/lib/vector"

# Ingest
sources:
  traefik_access:
    type: file
    ignore_older_secs: 600
    include:
      - "/var/log/traefik/access-traefik.log"
    read_from: "beginning"
transforms:
  tr_traefik_access_logs:
    type: remap
    inputs:
      - traefik_access
    source: |-
      . = parse_json!(.message)
sinks:
  loki_traefik_access_logs:
    type: loki
    labels:
      instance: "{{ ansible_hostname }}"
      type: "traefik_access_logs"
    inputs:
      - tr_traefik_access_logs
    endpoint: http://{{ vector_loki_endpoint }}
    encoding:
      codec: "json"
```

-----------------------------------------------------------------------------------------------------------                                       

# Vector : installation avec ansible

<br>

Add role in traefik & base playbooks

```
    vector_list: ["syslog"]

    vector_list: ["traefik"]
```