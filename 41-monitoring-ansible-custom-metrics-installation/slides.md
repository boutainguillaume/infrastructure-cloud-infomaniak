%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Custom Metrics - Backups consul


<br>

Example : consul backups on Object Storage


Project :

    * storage consul datas /data/var/lib/consul

    * in a container

    * with restic : deduplication, organization, retention...

    * ansible automation

    * dashboard & alerting

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Custom Metrics


<br>

Create a new role

Add variables

```
restic_s3_key: 
restic_s3_secret_key: 
restic_s3_secret_password: 
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Custom Metrics


<br>

Create a new role


Install restic


```
- name: install restic
  apt:
    name: restic
    state: present
    update_cache: yes
    cache_valid_time: 3600
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Custom Metrics


<br>

Create directory


```
- name: install restic
  file:
    path: "{{ item.dir }}"
    state: directory
    owner: root
    group: root
    mode: "{{ item.mode }}"
  loop:
    - { dir: "/root/.config/restic/", mode: "0750" }
    - { dir: "/opt/scripts/backups", mode: "0750" }
    - { dir: "/var/log/backups/", mode: "0755" }
    - { dir: "/var/lib/cron/", mode: "0755" }
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Custom Metrics


<br>

Install the configuration file

```
- name: add restic configuration file
  copy:
    content: |
      export AWS_DEFAULT_REGION="us-east-1"
      export RESTIC_REPOSITORY="s3:https://s3.pub1.infomaniak.cloud/backups-consul"
      export AWS_ACCESS_KEY_ID="{{ restic_s3_key }}"
      export AWS_SECRET_ACCESS_KEY="{{ restic_s3_secret_key }}"
      export RESTIC_PASSWORD="{{ restic_s3_secret_password }}"
    dest: /root/.config/restic/restic_consul.creds
    owner: root
    group: root
    mode: 0700
```

Note : we'll see later how to generelize our role

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Custom Metrics


<br>

Install the configuration file

```
- name: install the consul backup script
  template:
    src: backup_consul.sh.j2
    dest: /opt/scripts/backups/backup_consul.sh
    owner: root
    group: root
    mode: 0750
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Custom Metrics


<br>

Script details

```
LOG=/var/log/backups/restic_consul-$(date +%F).log
touch /var/lib/cron/start_consul_backup.txt
source /root/.config/restic/restic_consul.creds
restic init
/usr/local/bin/consul snapshot save /tmp/consul-backup.bck
restic backup /tmp/consul-backup.bck
restic forget \ 
    --keep-last=14 \
    --keep-within-daily=30d \
    --keep-within-weekly=90d \
    --keep-within-monthly 365d \
    --group-by '' \
    --prune \
    --host consul3
touch /var/lib/cron/end_consul_backup.txt
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Custom Metrics


<br>

Cron for crontab

```
template:
  src: backup_consul.j2
  dest: /etc/cron.d/backup_consul
  owner: root
  group: root
  mode: 0750
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Custom Metrics


<br>

Cron job file

```
00 1 * * * root /opt/scripts/backups/backup_consul.sh

```
